package com.ingress.kafkaconsumer.repository;

import com.ingress.kafkaconsumer.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}
